package uteis;

public class Usuario {

    private  String username;
    private  String password;

    public Usuario(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void getUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void getPassword (String password) {
        this.password = password;
    }
}
