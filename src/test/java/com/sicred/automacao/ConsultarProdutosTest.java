package com.sicred.automacao;

import org.junit.Test;
import uteis.Basetest;

import static io.restassured.RestAssured.*;

public class ConsultarProdutosTest extends Basetest {

    @Test
    public void consultarrProdutosSucesso() {

        given()
                .when()
                .get("/products")
                .then()
                .statusCode(200)
                .log().all();

    }


    @Test
    public void consultarProdutosInvalido() {

        given()
                .when()
                .get("/produc")
                .then()
                .statusCode(404)
                .log().all();

    }

}
