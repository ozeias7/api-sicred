package com.sicred.automacao;

import org.hamcrest.Matchers;
import org.junit.Test;
import uteis.Basetest;

import static io.restassured.RestAssured.*;

public class BucarUsuarioTest extends Basetest {

    @Test
    public void testBucarListaUsuarios(){
        given()
                //.contentType(ContentType.JSON)
                .when()
                .get("/users")
                .then()
                .log().all()
                .statusCode(200)
                .body("users.firstName", Matchers.hasItems("Terry","Sheldon","Terrill","Miles","Mavis","Alison","Oleta","Ewell","Demetrius","Eleanora"
                        ,"Marcel","Assunta","Trace","Enoch","Jeanne","Trycia","Bradford","Arely","Gust","Lenna","Doyle","Tressa","Felicity","Jocelyn","Edwina",
                        "Griffin","Piper","Kody","Macy","Maurine")).log().all();
        ;
    }


    @Test
    public void deveBuscarListaUserUm() {
        baseURI = "https://dummyjson.com";
        port = 443;

        given()
                .when()
                .get("/users")
                .then()
                .statusCode(200)
                .body("users.username", Matchers.hasItems("atuny0","hbingley1"))
                .body("users.password", Matchers.hasItem("9uQFF1Lh")).log().all();
    }


}
