package com.sicred.automacao;


import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import uteis.Basetest;
import uteis.Usuario;

import static io.restassured.RestAssured.*;
import uteis.Basetest;

public class BuscarProdutoComAutenticacaoTest extends Basetest {

    @Test
    public void testConsultaProdutoSucesso() {

        Usuario user = new Usuario("kminchelle", "0lelplR");
        String token=	given()
                .contentType("application/json; charset=UTF-16")
                .body(user)
                .when()
                .post("/auth/login")
                .then()
                .statusCode(200)
                .extract()
                .path("token");

        System.out.println(token);


        given()
                .contentType(ContentType.JSON)

                .header("Authorization", token)
                .when()
                .get("auth/products")
                .then()
                .log().all()

        ;
    }

    @Test
    public void testeConsultaProdutoSemToken() {

        given()
                .contentType(ContentType.JSON)
                .when()
                .get("auth/products")
                .then()
                .log().all()
                .statusCode(403)

        ;

    }
}
