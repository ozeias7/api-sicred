package com.sicred.automacao;

import io.restassured.http.ContentType;
import org.junit.Test;
import uteis.Basetest;
import uteis.Produtos;

import static io.restassured.RestAssured.*;

public class CadastrarProdutosTest extends Basetest {

    @Test
    public void cadastrarProdutoSucesso(){

        Produtos produtos = new Produtos();
        produtos.setTitle("Perfume Oil");
        produtos.setDescription("Mega Discount, Impression of A...");
        produtos.setPrice("9.0");
        produtos.setDiscountPercentage("8.4");
        produtos.setRating("4.29");
        produtos.setStock("4.29");
        produtos.setBrand("65");
        produtos.setCategory("fragrancess");
        produtos.setThumbnail("https://i.dummyjson.com/data/products/11/thumnail.jpg");



        given()
                .contentType(ContentType.JSON)
                .body(produtos)
                .when()
                .post("products/add")
                .then()
                .statusCode(200)
                .log().all();

    }

}
