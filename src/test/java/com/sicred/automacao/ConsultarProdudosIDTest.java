package com.sicred.automacao;

import org.junit.Test;
import uteis.Basetest;

import static io.restassured.RestAssured.*;


public class ConsultarProdudosIDTest extends Basetest {

    @Test
    public void cconsultarrProdutoIDSucesso(){

        given()
                .when()
                .get("products/1")
                .then()
                .statusCode(200)
                .log().all();

    }

    @Test
    public void cconsultarrProdutoIDErrado(){
        baseURI = "https://dummyjson.com";
        port = 443;

        given()
                .when()
                .get("products/0")
                .then()
                .statusCode(404)
                .log().all();

    }

}

