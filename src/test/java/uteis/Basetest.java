package uteis;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.regex.Matcher;

public class Basetest implements Constantes {

    @BeforeClass
    public static void setaup() {
        RestAssured.baseURI = APP_BASE_URL;
        RestAssured.port = APP_PORT;

        ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
        resBuilder.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
        RestAssured.responseSpecification = resBuilder.build();


        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
