package uteis;

import io.restassured.http.ContentType;

public interface Constantes {

    String   APP_BASE_URL = "https://dummyjson.com";
    Integer  APP_PORT  = 443;

    Long MAX_TIMEOUT = 3000L;
}
